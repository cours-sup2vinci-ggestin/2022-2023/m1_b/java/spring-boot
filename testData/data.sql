INSERT INTO sv_m1_superhero (id, superhero_name, secret_identity)
VALUES (1, 'Superman', 'Clark Kent'),
       (2, 'Batman', 'Bruce Wayne'),
       (3, 'Spider-Man', 'Peter Parker'),
       (4, 'Ironman', 'Tony Stark'),
       (5, 'Captain America', 'Steve Rogers'),
       (6, 'Thor', 'Thor Odinson'),
       (7, 'Hulk', 'Bruce Banner'),
       (8, 'Wolverine', 'James Howlett'),
       (9, 'Deadpool', 'Wade Wilson');

INSERT INTO sv_m1_power (name)
VALUES ('Vol'),
       ('Super force'),
       ('Richesse'),
       ('Super detective'),
       ('Super intelligence'),
       ('Regeneration'),
       ('Immortel');

INSERT INTO sv_m1_superhero_powers
VALUES (1, 1),
       (1, 2),
       (2, 3),
       (2, 4),
       (3, 2),
       (3, 5),
       (4, 3),
       (4, 5),
       (5, 2),
       (5, 5),
       (6, 2),
       (7, 2),
       (8, 6),
       (9, 7);
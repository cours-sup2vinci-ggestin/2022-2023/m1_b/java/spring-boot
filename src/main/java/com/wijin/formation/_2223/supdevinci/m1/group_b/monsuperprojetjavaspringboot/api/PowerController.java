package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.*;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.IdMismatchException;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.ResourceNotFoundException;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.mapper.PowerMapper;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.PowerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/v1/powers", produces = APPLICATION_JSON_VALUE)
public class PowerController {

    private final PowerService powerService;
    private final PowerMapper mapper;

    @GetMapping
    public ResponseEntity<PageDto<PowerDto>> getPowers(
            @RequestParam(required = false) String name,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "5") int pageSize
    ) {
        return ResponseEntity.ok(
                mapper.mapToPageDto(powerService.getAllPowers(name, PageRequest.of(page, pageSize)))
        );
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<PowerDto> getPowerById(
            @PathVariable(name = "id") Long powerId
    ) {
        return powerService.getPowerById(powerId)
                .map(mapper::mapToDto)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException("Power", powerId));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<PowerDto> updatePower(
            @PathVariable(name = "id") Long powerId,
            @RequestBody PowerDto powerDto
    ) {
        Optional<Power> powerOptional = powerService.getPowerById(powerId);
        if (powerOptional.isEmpty()) {
            throw new ResourceNotFoundException("Power", powerId);
        }

        if (!Objects.equals(powerId, powerDto.getId())) {
            throw new IdMismatchException(powerId, powerDto.getId());
        }

        Power power = powerOptional.get();
        mapper.mapToEntity(powerDto, power);
//        power.setName(powerDto.getPowerName());
//        power.setDescription(powerDto.getDescription());

        return ResponseEntity.ok(
                mapper.mapToDto(powerService.savePower(power))
        );
    }

    @PostMapping
    public ResponseEntity<PowerDto> createPower(
            @RequestBody @Valid PowerCreateDto powerCreateDto
    ) {
        Power power = mapper.mapToEntity(powerCreateDto);
        Power createdPower = powerService.savePower(power);
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.mapToDto(createdPower));
    }
}

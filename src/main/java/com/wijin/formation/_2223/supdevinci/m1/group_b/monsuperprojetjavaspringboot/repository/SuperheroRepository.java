package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.repository;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SuperheroRepository extends JpaRepository<Superhero, Long> {
    Page<Superhero> findAllBySuperheroNameStartingWithOrSecretIdentityStartingWith(String name, String identity, PageRequest page);
    Page<Superhero> findAllBySuperheroNameEndingWithOrSecretIdentityEndingWith(String name, String identity, PageRequest page);
    Page<Superhero> findAllBySuperheroNameOrSecretIdentity(String name, String identity, PageRequest page);
    Page<Superhero> findAllBySuperheroNameContainsOrSecretIdentityContains(String name, String identity, PageRequest page);
}

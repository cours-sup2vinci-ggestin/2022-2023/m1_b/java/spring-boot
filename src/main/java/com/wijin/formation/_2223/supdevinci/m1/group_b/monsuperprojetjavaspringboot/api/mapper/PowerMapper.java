package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.mapper;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PageDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PaginationContext;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PowerCreateDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PowerDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PageMapper.class})
public interface PowerMapper {

    Power mapToEntity(PowerCreateDto powerCreateDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "name", source = "powerName")
    void mapToEntity(PowerDto powerDto, @MappingTarget Power power);

    @Mapping(target = "powerName", source = "name")
//    @Mapping(target = "usedBy", expression = "java(power.getSuperheroes().size())")
    @Mapping(target = "usedBy", source = "superheroes")
    PowerDto mapToDto(Power power);

    default int getSuperheroesNumber(List<Superhero> superheroList) {
        return superheroList.size();
    }



    @Mapping(target = "data", source = "content")
    @Mapping(target = "context", source = ".")
    PageDto<PowerDto> mapToPageDto(Page<Power> page);

}

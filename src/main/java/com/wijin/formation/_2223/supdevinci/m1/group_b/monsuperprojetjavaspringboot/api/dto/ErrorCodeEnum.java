package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

public enum ErrorCodeEnum {
    RESOURCE_NOT_FOUND,
    ID_MISMATCH
}

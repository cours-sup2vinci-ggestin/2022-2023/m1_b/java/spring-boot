package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.impl;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.repository.PowerRepository;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.PowerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PowerServiceImpl implements PowerService {

    public final PowerRepository powerRepository;

    @Override
    public Page<Power> getAllPowers(String name, PageRequest page) {
        if (name == null) {
            return powerRepository.findAll(page);
        } else {
            if (name.startsWith("*") && name.endsWith("*")) {
                return powerRepository.findAllByNameContains(name.substring(1, name.length() - 1), page);
            } else if (name.startsWith("*")) {
                return powerRepository.findAllByNameEndingWith(name.substring(1), page);
            } else if (name.endsWith("*")) {
                return powerRepository.findAllByNameStartingWith(name.substring(0, name.length() - 1), page);
            } else {
                return powerRepository.findAllByName(name, page);
            }
        }
    }

    @Override
    public Optional<Power> getPowerById(Long id) {
        return powerRepository.findById(id);
    }

    @Override
    public Power savePower(Power power) {
        return powerRepository.save(power);
    }
}

package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.mapper;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PageDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PaginationContext;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.SuperheroCreateDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.SuperheroDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring", uses = {PowerMapper.class, PageMapper.class})
public interface SuperheroMapper {

    Superhero mapToEntity(SuperheroCreateDto SuperheroCreateDto);
    SuperheroDto mapToDto(Superhero Superhero);

    @Mapping(target = "data", source = "content")
    @Mapping(target = "context", source = ".")
    PageDto<SuperheroDto> mapToPageDto(Page<Superhero> page);
}

package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

public interface SuperheroService {
    Page<Superhero> getAllSuperheroes(String name, PageRequest page);

    Optional<Superhero> getSuperheroById(Long id);

    Superhero saveSuperhero(Superhero superhero);
}

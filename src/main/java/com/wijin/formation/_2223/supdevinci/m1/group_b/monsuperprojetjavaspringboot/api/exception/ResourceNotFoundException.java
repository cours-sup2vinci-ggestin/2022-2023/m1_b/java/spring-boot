package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String resourceType, Long id) {
        super(resourceType + " with id [" + id + "] not found");
    }
}

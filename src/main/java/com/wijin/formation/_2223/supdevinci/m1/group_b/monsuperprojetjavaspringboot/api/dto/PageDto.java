package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class PageDto<T> {
    private List<T> data;
    private PaginationContext context;
}

package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class PowerCreateDto {
    @NotNull
    @NotEmpty
    private String name;
    private String description;
}

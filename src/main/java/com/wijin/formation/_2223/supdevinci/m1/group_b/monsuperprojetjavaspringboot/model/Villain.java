package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sv_m1_villain")
@Getter
@Setter
@NoArgsConstructor
public class Villain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToMany(mappedBy = "villains")
    private List<Superhero> superheroes;
    @OneToOne(mappedBy = "nemesis")
    private Superhero nemesis;
}

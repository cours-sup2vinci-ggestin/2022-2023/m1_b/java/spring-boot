package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PaginationContext {
    private Integer page;
    private Integer pageSize;
    private Integer count;
    private Long total;
}

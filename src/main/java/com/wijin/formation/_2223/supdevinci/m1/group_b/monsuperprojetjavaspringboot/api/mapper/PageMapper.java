package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.mapper;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PageDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PaginationContext;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PowerCreateDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.PowerDto;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface PageMapper {

    @Mapping(target = "page", source = "number")
    @Mapping(target = "count", source = "numberOfElements")
    @Mapping(target = "pageSize", source = "size")
    @Mapping(target = "total", source = "totalElements")
    PaginationContext toPaginationContext(Page<?> page);
}

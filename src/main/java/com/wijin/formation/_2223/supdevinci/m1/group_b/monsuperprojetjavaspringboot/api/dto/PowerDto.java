package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PowerDto {
    private Long id;
    private String powerName;
    private String description;
    private Integer usedBy;
}

package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SuperheroDto {
    private Long id;
    private String superheroName;
    private String secretIdentity;
    private List<PowerDto> powers;
}

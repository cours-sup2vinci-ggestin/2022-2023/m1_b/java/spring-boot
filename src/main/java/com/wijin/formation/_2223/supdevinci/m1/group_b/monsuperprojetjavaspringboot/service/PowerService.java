package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

public interface PowerService {
    Page<Power> getAllPowers(String name, PageRequest page);

    Optional<Power> getPowerById(Long id);

    Power savePower(Power power);
}

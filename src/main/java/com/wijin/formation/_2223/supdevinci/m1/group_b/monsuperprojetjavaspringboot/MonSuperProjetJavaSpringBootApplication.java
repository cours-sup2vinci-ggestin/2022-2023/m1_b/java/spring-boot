package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonSuperProjetJavaSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonSuperProjetJavaSpringBootApplication.class, args);
    }

}

package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.ApiError;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.ErrorCodeEnum;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.IdMismatchException;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class MyControllerAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {ResourceNotFoundException.class})
    protected ApiError handleNotFound(ResourceNotFoundException ex, WebRequest request) {
        return ApiError.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .status(HttpStatus.NOT_FOUND.getReasonPhrase())
                .message(ex.getMessage())
                .error(ErrorCodeEnum.RESOURCE_NOT_FOUND)
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {IdMismatchException.class})
    protected ApiError handleBadRequest(IdMismatchException ex, WebRequest request) {
        return ApiError.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .message(ex.getMessage())
                .error(ErrorCodeEnum.ID_MISMATCH)
                .build();
    }
}

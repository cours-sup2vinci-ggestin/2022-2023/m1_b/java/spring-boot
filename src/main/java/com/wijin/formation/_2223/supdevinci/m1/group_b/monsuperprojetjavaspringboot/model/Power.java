package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sv_m1_power")
@Getter
@Setter
@NoArgsConstructor
public class Power {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    @Column(unique = true)
    private String name;
    @ManyToMany(mappedBy = "powers")
    private List<Superhero> superheroes;
}

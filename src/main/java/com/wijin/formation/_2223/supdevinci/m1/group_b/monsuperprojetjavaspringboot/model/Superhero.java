package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "sv_m1_superhero")
@ToString
public class Superhero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String superheroName;
    private String secretIdentity;
    @ManyToMany
    @JoinTable(name = "sv_m1_superhero_powers")
    private List<Power> powers;
    @ManyToMany
    @JoinTable(name = "sv_m1_superhero_villains")
    private List<Villain> villains;
    @OneToOne
    private Villain nemesis;
    @OneToMany(mappedBy = "mentor")
    private List<Superhero> sidekicks;
    @ManyToOne
    private Superhero mentor;
}

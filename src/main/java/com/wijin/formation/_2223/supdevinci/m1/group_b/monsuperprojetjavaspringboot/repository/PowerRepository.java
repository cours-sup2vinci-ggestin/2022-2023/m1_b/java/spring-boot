package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.repository;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Power;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PowerRepository extends JpaRepository<Power, Long> {
    List<Power> findAllByNameStartingWith(String name);
    Page<Power> findAllByNameStartingWith(String name, Pageable page);
    Page<Power> findAllByNameContains(String name, Pageable page);
    Page<Power> findAllByNameEndingWith(String name, Pageable page);
    Page<Power> findAllByName(String name, Pageable page);
}

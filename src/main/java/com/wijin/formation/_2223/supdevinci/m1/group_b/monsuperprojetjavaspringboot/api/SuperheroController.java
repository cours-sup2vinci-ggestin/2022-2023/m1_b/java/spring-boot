package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.dto.*;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.IdMismatchException;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.exception.ResourceNotFoundException;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.api.mapper.SuperheroMapper;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.SuperheroService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/v1/superheroes", produces = APPLICATION_JSON_VALUE)
public class SuperheroController {

    private final SuperheroService superheroService;

    private final SuperheroMapper mapper;
    
    @GetMapping
    public ResponseEntity<PageDto<SuperheroDto>> getSuperheroes(
            @RequestParam(required = false) String name,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "5") int pageSize
    ) {
        return ResponseEntity.ok(
                mapper.mapToPageDto(superheroService.getAllSuperheroes(name, PageRequest.of(page, pageSize)))
        );
    }
    @PostMapping
    public ResponseEntity<SuperheroDto> createSuperhero(
            @RequestBody SuperheroCreateDto superheroCreateDto
    ) {
        Superhero superhero = mapper.mapToEntity(superheroCreateDto);
        Superhero createdSuperhero = superheroService.saveSuperhero(superhero);
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.mapToDto(createdSuperhero));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<SuperheroDto> updateSuperhero(
            @PathVariable Long id,
            @RequestBody SuperheroDto superheroDto
    ) {
        Optional<Superhero> optionalSuperhero = superheroService.getSuperheroById(id);
        if (optionalSuperhero.isEmpty()) {
            throw new ResourceNotFoundException("Superhero", id);
        }
        
        if (!Objects.equals(id, superheroDto.getId())) {
            throw new IdMismatchException(id, superheroDto.getId());
        }
        
        Superhero superhero = optionalSuperhero.get();
        superhero.setSuperheroName(superheroDto.getSuperheroName());
        superhero.setSecretIdentity(superheroDto.getSecretIdentity());
        
        Superhero updatedSuperhero = superheroService.saveSuperhero(superhero);
        return ResponseEntity.ok(mapper.mapToDto(updatedSuperhero));
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<SuperheroDto> getSuperheroById(
            @PathVariable(name = "id") Long superheroId
    ) {
        return superheroService.getSuperheroById(superheroId)
                .map(mapper::mapToDto)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException("Superhero", superheroId));
    }
}


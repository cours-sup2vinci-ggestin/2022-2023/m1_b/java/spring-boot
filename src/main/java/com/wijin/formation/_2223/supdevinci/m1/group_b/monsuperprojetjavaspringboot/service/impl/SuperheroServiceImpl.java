package com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.impl;

import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.model.Superhero;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.repository.SuperheroRepository;
import com.wijin.formation._2223.supdevinci.m1.group_b.monsuperprojetjavaspringboot.service.SuperheroService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SuperheroServiceImpl implements SuperheroService {

    public final SuperheroRepository superheroRepository;

    @Override
    public Page<Superhero> getAllSuperheroes(String name, PageRequest page) {
        if (name == null) {
            return superheroRepository.findAll(page);
        } else {
            if (name.startsWith("*") && name.endsWith("*")) {
                return superheroRepository.findAllBySuperheroNameContainsOrSecretIdentityContains(name.substring(1, name.length() - 1), name.substring(1, name.length() - 1), page);
            } else if (name.startsWith("*")) {
                return superheroRepository.findAllBySuperheroNameEndingWithOrSecretIdentityEndingWith(name.substring(1), name.substring(1), page);
            } else if (name.endsWith("*")) {
                return superheroRepository.findAllBySuperheroNameStartingWithOrSecretIdentityStartingWith(name.substring(0, name.length() - 1), name.substring(0, name.length() - 1), page);
            } else {
                return superheroRepository.findAllBySuperheroNameOrSecretIdentity(name, name, page);
            }
        }
    }

    @Override
    public Optional<Superhero> getSuperheroById(Long id) {
        return superheroRepository.findById(id);
    }

    @Override
    public Superhero saveSuperhero(Superhero superhero) {
        return superheroRepository.save(superhero);
    }
}
